﻿using UnityEngine;
using System.Collections;
using GameSparks.RT;

public class GSRTManager : MonoBehaviour {

	private static GSRTManager instance = null;
	public static GSRTManager GetInstance() {
		return instance;
	}

	void Awake()
	{
		instance = this;
	}

	public void UpdateOpponentTransforms(RTPacket _packet)
	{
        Debug.Log("GSRTManager| Recieved movement info packet from P" + _packet.Sender + ".");
		var playerList = ScriptsPointer.GameController.playerList;
        bool foundPlr = false;
		foreach (var plr in playerList) {
			if (plr.playerIndex == (_packet.Sender - 1)) {
                foundPlr = true;
                Debug.Log("GSRTManager| Found packet sender in PlayerList");
				var plrDataListener = plr.GetComponent<OpponentRTDataListener> ();

                float plrPosX = _packet.Data.GetVector4(1).Value.x;
                float plrPosY = _packet.Data.GetVector4(1).Value.y;
                Vector2 plrPos = new Vector2(plrPosX, plrPosY);

                float plrVelX = _packet.Data.GetVector4(1).Value.z;
                float plrVelY = _packet.Data.GetVector4(1).Value.w;
                Vector2 plrVelocity = new Vector2(plrVelX, plrVelY);

                float plrRot = _packet.Data.GetFloat(2).Value;

				int acc = _packet.Data.GetInt (3).Value;

                plrDataListener.UpdatePlayerMovement (plrPos, plrVelocity, plrRot);
				plrDataListener.accelerating = (acc == 1 ? true : false);
                break;
			}
		
		}
        if (!foundPlr)
            Debug.LogError("GSRTManager| Player matching packet sender not found.");
	}

	public void RegisterOpponentLanded(RTPacket _packet)
	{
		var playerList = ScriptsPointer.GameController.playerList;
		foreach (var plr in playerList) {
            if (plr.playerIndex == (_packet.Sender - 1))
            {
                var opp = plr.GetComponent<OpponentRTDataListener>();
                opp.UpdatePlayerLanded(plr.playerIndex, plr.teamIndex);
                break;
			}
		}
	}

	public void RegisterOpponentCollision(RTPacket _packet)
	{
        var playerList = ScriptsPointer.GameController.playerList;
        foreach (var plr in playerList)
        {
            if (plr.playerIndex == (_packet.Sender - 1))
            {
                var opp = plr.GetComponent<OpponentRTDataListener>();
                opp.UpdatePlayerCollided(_packet.Data.GetInt(1).Value);
                break;
            }
        }
    }

	public void RegisterOpponentDied(RTPacket _packet)
	{
        var playerList = ScriptsPointer.GameController.playerList;
        foreach (var plr in playerList)
        {
            if (plr.playerIndex == (_packet.Sender - 1))
            {
                var opp = plr.GetComponent<OpponentRTDataListener>();
                opp.UpdatePlayerDied();
                break;
            }
        }
    }

    public void RegisterOpponentWarped(RTPacket _packet)
    {
        var playerList = ScriptsPointer.GameController.playerList;
        foreach (var plr in playerList)
        {
            if (plr.playerIndex == (_packet.Sender - 1))
            {
                Vector2 warpPos = _packet.Data.GetVector2(1).Value;
                plr.GetComponent<OpponentRTDataListener>().UpdatePlayerWarped(warpPos);
                break;
            }
        }
    }

	public void OnOpponentDisconnected(RTPacket _packet)
	{
		
	}
}
