﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GSNetworking.ApiManagers;
using GSNetworking.DataTypes.RealtimeData;
using GSNetworking.DataTypes;

public class GameController : MonoBehaviour {

	public Transform messages;
	public GameObject charPrefab;
	public List<PlayerBaseInfo> playerList;
	public List<Transform> spawnPoints;
    public Platform platform;

    private bool roundOver;

	private Coroutine timeTick;
    public void StopTime() {
        if (timeTick != null)
            StopCoroutine(timeTick);
        timeTick = null;
    }

	// Use this for initialization
	void Start () {
		ScriptsPointer.MatchData.SetupTeams ();
		InitMatch ();
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			RealtimeService.Api.Disconnect ();
			UnityEngine.SceneManagement.SceneManager.LoadScene ("StartScene");
		}
	}
	
	private void InitMatch()
	{
        roundOver = false;
		SpawnPlayers();
        ScriptsPointer.HUDController.SetTextColors();
		foreach (var plr in playerList)
			plr.SetControlActive (false);
		
		StartCoroutine (PrepareRound ());
	}

	private IEnumerator PrepareRound()
	{
        roundOver = false;
        Debug.Log("GameController| Preparing round " + ScriptsPointer.MatchData.GetRoundNumber());

        Team plrTeam = playerList[(int)RealtimeService.Api.PeerId - 1].GetTeam();
        ScriptsPointer.HUDController.UpdateUIRoleMessage(plrTeam);
        ScriptsPointer.HUDController.UpdateRoleUI();
		ScriptsPointer.HUDController.UpdateMatchpointUI();

        MessageManager.ShowMessage ("RoundStart");

        string r_label = "Attack";
        if (plrTeam.teamRole == Team.Role.DEF)
            r_label = "Defend";

        FXUtil.CreateVisualEffect("Role" + r_label);
		yield return new WaitForSeconds (4f);

		BeginRound();
	}

	public void BeginRound()
	{
		timeTick = StartCoroutine (TimeTick ());
		foreach (var plr in playerList) {
			plr.SetControlActive (true);
			if (plr.isLocal)
				plr.GetComponent<PlayerMovementController> ().StartSync ();
		}
	}

	public IEnumerator EndRound()
	{
        Debug.Log("GameController| Round ended!");
        LockAllPlayersControl();
        StopTime();

        yield return new WaitForSeconds(1f);

        platform.Reset();
        ResetAllPlayers();
		ScriptsPointer.MatchData.NewRound();
        ScriptsPointer.HUDController.ResetAndUpdateAllUI();
		StartCoroutine(PrepareRound ());
	}

    private void LockAllPlayersControl()
    {
        foreach (var plr in playerList)
            plr.SetControlActive(false);
    }

    private void ResetAllPlayers()
    {
        foreach (var plr in playerList)
        {
            if (plr.isLocal)
                plr.GetComponent<PlayerMovementController>().Reset();
            else
                plr.GetComponent<OpponentRTDataListener>().Reset();
        }
    }

	public void EndMatch()
	{
		// show winner message
		StartCoroutine(DisconnectFromSession());
	}

	private IEnumerator DisconnectFromSession()
	{
		yield return new WaitForSeconds (1f);
		RealtimeService.Api.Disconnect ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("StartScene");
	}

	public void SpawnPlayers()
	{
		int tIndex = 0;

		foreach (var plr in RealtimeService.SessionInfo.GetPlayers()) {
			int pIndex = (plr.StatsData.PeerId - 1);
			PlayerBaseInfo newPlr = (Instantiate (charPrefab, spawnPoints [pIndex].position, spawnPoints [pIndex].rotation) as GameObject)
				.GetComponent<PlayerBaseInfo>();
			
			newPlr.gameObject.name = "P" + plr.StatsData.PeerId + " " + plr.DisplayName;
			newPlr.displayName = plr.DisplayName;
			newPlr.playerIndex = pIndex;
			newPlr.teamIndex = tIndex;
			newPlr.SetCharColor (GameData.CharacterSkinColor [pIndex]);
			newPlr.SetGlowColor (GameData.TeamColor [tIndex]);
            newPlr.glowEffect.Play();

			if (plr.StatsData.PeerId == RealtimeService.Api.PeerId) {
				newPlr.isLocal = true;
				newPlr.gameObject.name += " (Local)";
			}
			else
			{
				newPlr.isLocal = false;
				newPlr.SetCharOpacity(0.66f);
				newPlr.SetGlowOpacity(0.66f);
			}

			newPlr.SetupMovementControl();
			playerList.Add (newPlr);
            newPlr.GetTeam().playersAlive++;
			newPlr.GetTeam ().totalPlayersAmount = newPlr.GetTeam ().playersAlive;

			if (tIndex == 0) tIndex = 1;
			else tIndex = 0;
		}
	}

	public void Score(int teamId)
	{
        if (!roundOver)
        {
            roundOver = true;
            Team pTeam = ScriptsPointer.MatchData.GetTeam(teamId);
            pTeam.score++;

            if (RealtimeService.SessionInfo.IsCreator)
                Analytics.RecordTeamRoleWin(Team.roleLabel[pTeam.RoleIndex]);

            ScriptsPointer.HUDController.UpdateScoreUI(teamId, pTeam.score);
            ScriptsPointer.HUDController.UpdateMatchpointUI();

            if (ScriptsPointer.MatchData.GetTeam(0).score < 3 &&
                ScriptsPointer.MatchData.GetTeam(1).score < 3)
                StartCoroutine(EndRound());
            else
                EndMatch();
        }
	}

	private IEnumerator TimeTick()
	{
		yield return new WaitForSeconds(1f);
		ScriptsPointer.MatchData.TickTime();
		ScriptsPointer.HUDController.UpdateTimeUI();

        if (ScriptsPointer.MatchData.GetTimeLeft() > 0)
			timeTick = StartCoroutine(TimeTick());

        else
			ScriptsPointer.GameController.Score(ScriptsPointer.MatchData.GetTeamByRole(Team.Role.DEF).teamIndex);
	}

	public void ShowMessage(string msgObj)
	{
		messages.Find (msgObj).GetComponent<Animator>().Play(msgObj, 0, 0f);
	}
}
