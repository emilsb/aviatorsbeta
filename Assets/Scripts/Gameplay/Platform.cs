﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

    public SpriteRenderer[] lamp = new SpriteRenderer[2];
    public ParticleSystem[] glow = new ParticleSystem[3];
	public ParticleSystem captureEffect;
	public bool isCaptured;
	public int capturerTeamId;
	public int capturerPlayerId;

	public void Capture(int _playerId, int _teamId)
	{
        Debug.Log("Platform| Captured by P" + (_playerId + 1) + "!");
		isCaptured = true;
		capturerPlayerId = _playerId;
		capturerTeamId = _teamId;

		captureEffect.startColor = GameData.TeamColor [_teamId];
		foreach (var fx in captureEffect.GetComponentsInChildren<ParticleSystem>())
			fx.startColor = GameData.TeamColor [_teamId];
		captureEffect.Play (true);

		captureEffect.GetComponent<AudioSource> ().Play ();
		foreach (var snd in captureEffect.GetComponentsInChildren<AudioSource>())
			snd.Play ();

		foreach (var rend in lamp)
			rend.color = GameData.TeamColor [_teamId];
		
        foreach (var part in glow) {
            part.Stop();
            part.startColor = GameData.TeamColor[_teamId];
            if (part.name == "LampGlow")
            {
                Color newCol = part.startColor;
                newCol.a = 0.4f;
                part.startColor = newCol;
            }
            part.Play();
        }

		ScriptsPointer.GameController.Score (_teamId);
	}

    public void Reset()
    {
        isCaptured = false;
        capturerPlayerId = 0;
        capturerTeamId = 0;

        foreach (var rend in lamp)
            rend.color = Color.white;
        foreach (var part in glow)
        {
            part.Stop();
            part.startColor = part.startColor = Color.white; ;
            if (part.name == "LampGlow")
            {
                Color newCol = part.startColor;
                newCol.a = 0.4f;
                part.startColor = newCol;
            }
            part.Play();
        }
    }
}
