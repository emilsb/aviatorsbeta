﻿using UnityEngine;
using System.Collections;
using GameSparks.RT;

public class WarpPoint : MonoBehaviour {

    public Transform warpTarget;
    public bool warpX, warpY;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var plrInfo = other.gameObject.GetComponent<PlayerBaseInfo>();
            if (plrInfo.isLocal)
            {
                Warp(plrInfo.gameObject);
            }
        }
    }


    void Warp(GameObject obj)
    {
        Vector2 prevPos = obj.transform.position;
        Vector2 newPos = warpTarget.position;

        if (!warpX)
            newPos.x = prevPos.x;
        if (!warpY)
            newPos.y = prevPos.y;

        obj.transform.position = newPos;
        obj.GetComponent<PlayerMovementController>().BroadcastWarped(newPos);
    }
}
