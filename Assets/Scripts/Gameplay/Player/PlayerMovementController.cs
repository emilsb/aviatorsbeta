﻿using UnityEngine;
using System.Collections;
using GameSparks.RT;
using GSNetworking.ApiManagers;

public class PlayerMovementController : MonoBehaviour {

	public PlayerBaseInfo baseInfo;
	private float accelerationForce = GameData.PlayerAccelerationForce;
	private float rotateSpeed = GameData.PlayerRotateSpeed;
	private float syncUpdateRate = GameData.PlayerSyncUpdateRate;

	private bool accelerating;
	private bool rotating;
    private bool onPlatform;
    private Vector2 faceDir;
	private Vector2 moveDir;
    
	private Vector3 prevPos, velocity;
    private float speed;

    private Coroutine movementSync;
    private Coroutine lockingPlatform;
    
	public void StartSync()
	{
		movementSync = StartCoroutine (MovementSync ());
	}
	public void StopSync()
	{
		if (movementSync != null)
			StopCoroutine (movementSync);
		
		movementSync = null;
	}

	private void UpdateAcceleration()
	{
		if (accelerating) {
			if (moveDir != faceDir)
				moveDir = faceDir;

			baseInfo.physics.AddForce (moveDir * (accelerationForce * 100f) * Time.deltaTime);

			if (baseInfo.smokeTrail.isStopped)
				baseInfo.smokeTrail.Play ();
		}
		else
		{
			if (baseInfo.smokeTrail.isPlaying)
				baseInfo.smokeTrail.Stop ();
		}
	}

	private void UpdateRotation()
	{
		float horAxisValue = Input.GetAxis ("Horizontal");
		float rotation = -horAxisValue * (rotateSpeed * 360) * Time.deltaTime;
		baseInfo.physics.angularVelocity = rotation;
		faceDir = transform.TransformDirection(Vector2.up);
	}

	void Update()
	{
        //onPlatform = baseInfo.collider.IsTouching(ScriptsPointer.GameController.platform);
		if (baseInfo.controlActive) {
			// Acceleration
			accelerating = Input.GetButton ("Accelerate");
			UpdateAcceleration ();


			// Rotation
			float horAxisValue = Input.GetAxis ("Horizontal");
			rotating = (horAxisValue < 0f || horAxisValue > 0f);
			UpdateRotation ();
		}
		else {
			if (baseInfo.smokeTrail.isPlaying)
				baseInfo.smokeTrail.Stop ();
		}

		velocity = transform.position - prevPos;
        speed = velocity.magnitude;
	}

	private IEnumerator MovementSync()
	{
		BroadcastMovement ();
		yield return new WaitForSeconds (syncUpdateRate);
		movementSync = StartCoroutine (MovementSync ());
	}

	public void Reset()
	{
		StopSync ();
        lockingPlatform = null;

		faceDir = Vector2.zero;
		moveDir = Vector2.zero;
		velocity = Vector2.zero;
		prevPos = Vector3.zero;
        onPlatform = false;

		baseInfo.Reset ();
	}

	public void BroadcastMovement()
	{
		if (transform.position != prevPos || accelerating || rotating) {
			using (RTData data = RTData.Get ()) {
				data.SetVector4 (1, new Vector4 (transform.position.x, transform.position.y, velocity.x, velocity.y));
				data.SetFloat (2, transform.eulerAngles.z);

				int acc = (accelerating ? 1 : 0);
				data.SetInt (3, acc);

				RealtimeService.Api.SendData (
					20, GameSparksRT.DeliveryIntent.UNRELIABLE_SEQUENCED, data
				);
			}

			prevPos = transform.position;
		}
	}

	public void BroadcastLanded()
	{
		using (RTData data = RTData.Get ()) {
			RealtimeService.Api.SendData (22, GameSparksRT.DeliveryIntent.RELIABLE, data);
		}
	}

	public void BroadcastCollision(int damage)
	{
		using (RTData data = RTData.Get()) {
			data.SetInt (1, damage);
			RealtimeService.Api.SendData(21, GameSparksRT.DeliveryIntent.RELIABLE, data);
		}
	}

	public void BroadcastDied()
	{
        using (RTData data = RTData.Get()) {
            RealtimeService.Api.SendData(23, GameSparksRT.DeliveryIntent.RELIABLE, data);
        }
	}

    public void BroadcastWarped(Vector2 targetPos)
    {
        using (RTData data = RTData.Get())
        {
            data.SetVector2(1, targetPos);
            RealtimeService.Api.SendData(24, GameSparksRT.DeliveryIntent.RELIABLE, data);
        }
    }

    private IEnumerator CapturingPlatform(Platform _platform)
    {
        yield return new WaitForSeconds(1f);
        if (!onPlatform || _platform.isCaptured)
        {
            Debug.Log("PlayerMovementController| Platform capture failed.");
            yield break;
        }
            
        else
        {
            Debug.Log("PlayerMovementController| Platform captured!");
            BroadcastLanded();
            baseInfo.SetControlActive(false);
            _platform.Capture(baseInfo.playerIndex, baseInfo.teamIndex);
        }
    }

    private void InterruptLanding()
    {
        if (lockingPlatform != null)
        {
            StopCoroutine(lockingPlatform);
            lockingPlatform = null;
        }
    }


    /*##############################>
	 * ##	Collisions handling
	 * ##
	 * ####################>
	 * */

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
			Platform platform = other.gameObject.GetComponent<Platform>();
			if (speed <= 0.3f)
				baseInfo.physics.velocity = Vector2.zero;

			if (!onPlatform)
				onPlatform = true;

			else {
				if (baseInfo.GetTeam().teamRole == Team.Role.ATK && !platform.isCaptured)
				{
					float plrRot = baseInfo.transform.rotation.z;
					if (baseInfo.physics.angularVelocity < 0.1f && lockingPlatform == null && plrRot > -0.01f && plrRot < 0.01f) {
						lockingPlatform = StartCoroutine (CapturingPlatform (platform));
						Debug.Log ("PlayerMovementController| Capturing platform.");
					}
				}
			}
        }
    }

    void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Platform"))
		{
			if (speed <= 0.3f)
				baseInfo.physics.velocity = Vector2.zero;

			if (CollisionDamage ()) {
                FXUtil.CreateVisualEffect("PlayerHit").SetPosition(transform.position);
                FXUtil.CreateSoundEffect("Hit");
            }
		}

        if (other.gameObject.CompareTag("Obstacle"))
        {
			if (CollisionDamage()) {
                FXUtil.CreateVisualEffect("PlayerHit").SetPosition(transform.position);
                FXUtil.CreateSoundEffect("Hit");
            }
        }

        if (other.gameObject.CompareTag("KillZone"))
        {
            baseInfo.Damage(100);
        }

        if (other.gameObject.CompareTag("Player"))
        {
            FXUtil.CreateVisualEffect("PlayerHit").SetPosition(transform.position);
            FXUtil.CreateSoundEffect("Hit");

            OpponentRTDataListener opp = other.gameObject.GetComponent<OpponentRTDataListener> ();
			CalculatePlayerHitDamage (opp);

			float plrVel = velocity.sqrMagnitude;
			float oppVel = opp.velocity.sqrMagnitude;

			if (plrVel <= 0.01f || onPlatform)
				baseInfo.physics.AddForceAtPosition (opp.moveDir * oppVel * 40f, opp.transform.position, ForceMode2D.Impulse);

			if (plrVel >= oppVel)
				baseInfo.physics.AddForce (-moveDir * plrVel * 40f, ForceMode2D.Impulse);
			else if (plrVel < oppVel)
				baseInfo.physics.AddForce (opp.moveDir * oppVel * 40f, ForceMode2D.Impulse);
        }
	}
	private void CalculatePlayerHitDamage(OpponentRTDataListener opp)
	{
		int oppSpeed = Mathf.Clamp(Mathf.RoundToInt(opp.speed), 1, 100);
		int plrSpeed = Mathf.Clamp(Mathf.RoundToInt(speed), 1, 100);

		if (plrSpeed < oppSpeed) {
			int dmg = Mathf.Clamp (Mathf.RoundToInt (oppSpeed - speed), 1, 100) * 10;
			baseInfo.Damage (dmg);
		}

		else if (plrSpeed == oppSpeed) {
			int dmg = (Mathf.Clamp (Mathf.RoundToInt (speed), 1, 100) * 10) / 2;
			baseInfo.Damage (dmg);
		}

		else if (plrSpeed > oppSpeed) { }
	}

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            InterruptLanding();
            onPlatform = false;
        }
    }

    private bool CollisionDamage()
    {
        if (speed > GameData.PlayerSpeedMaxCrash)
        {
            int dmg = 100;
            baseInfo.Damage(dmg);
            return true;
        }
        else if (speed > GameData.PlayerSpeedMinDamage)
        {
            int dmg = Mathf.Clamp(Mathf.RoundToInt(speed - GameData.PlayerSpeedMaxCrash), 1, 100) * 10;
            baseInfo.Damage(dmg);
            return true;
        }
        else return false;
    }
}