﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerBaseInfo : MonoBehaviour {

	public ParticleSystem glowEffect;
	public ParticleSystem smokeTrail;
	public Transform bodyParts;
	public MeshRenderer bodyRend, eyesRend;
	public Rigidbody2D physics;
    new public Collider2D collider;

	public bool onPlatform;
	public bool landed;
	public bool dead;
	public int health;
	public bool isLocal;
	public int playerIndex;
	public int teamIndex;
	public string displayName;
	public bool controlActive;
	public float syncUpdateRate = 0.1f;

	private int startHealth;
	private Vector3 startPos;

    private Text debugVelocity;

	void Start()
	{
		startHealth = health;
		startPos = transform.position;
    }

	public void SetupMovementControl()
	{
		if (isLocal) {
			var localScript = gameObject.AddComponent<PlayerMovementController>();
			localScript.baseInfo = this;
		} else {
			var oppScript = gameObject.AddComponent<OpponentRTDataListener>();
			oppScript.baseInfo = this;
		}
	}

	public Team GetTeam() {
		if (ScriptsPointer.MatchData == null) {
			Debug.LogError ("PlayerBaseInfo| Can't get team, MatchData reference not set.");
			return null;
		}
		else
			return ScriptsPointer.MatchData.GetTeam (teamIndex);
	}

	public void SetCharColor(Color newColor)
	{
		if (bodyRend != null)
			bodyRend.material.color = newColor;
		else
			Debug.LogError ("PlayerBaseInfo| Error setting body color, bodyRend not assigned.");
	}

	public void SetCharOpacity(float alpha)
	{
		if (bodyRend != null) {
			Color newBodyColor = bodyRend.material.color;
			newBodyColor.a = alpha;
			bodyRend.material.color = newBodyColor;
		}
		if (eyesRend != null) {
			Color newEyesColor = eyesRend.material.color;
			newEyesColor.a = alpha;
			eyesRend.material.color = newEyesColor;
		}
	}

	public void SetGlowColor(Color newColor)
	{
		if (glowEffect != null)
			glowEffect.startColor = newColor;
	}

	public void SetGlowOpacity(float alpha)
	{
		if (glowEffect != null) {
			Color newColor = glowEffect.startColor;
			newColor.a = alpha;
			glowEffect.startColor = newColor;
		}
	}

	public void SetControlActive(bool value)
	{
		controlActive = value;
        collider.enabled = value;
		if (physics != null)
			physics.isKinematic = !value;
	}

	public void SetVisible(bool value)
	{
		if (!value) {
			glowEffect.Clear ();
			glowEffect.Stop ();
			smokeTrail.Clear ();
			smokeTrail.Stop ();
		} else {
			glowEffect.Play ();
			smokeTrail.Play ();
		}

		foreach (var _rend in bodyParts.GetComponentsInChildren<MeshRenderer>())
			_rend.enabled = value;
		
		//bodyRend.enabled = value;
		//eyesRend.enabled = value;
        collider.enabled = value;
	}

	public void Reset()
	{
		health = startHealth;
		if (physics != null) {
			physics.velocity = Vector2.zero;
			physics.angularVelocity = 0;
		}

		transform.position = startPos;
		transform.eulerAngles = Vector3.zero;
		onPlatform = false;
		landed = false;
		dead = false;
		SetVisible (true);
		SetControlActive (false);
		ScriptsPointer.HUDController.UpdatePlayerHealthUI(playerIndex, startHealth);
	}

    public void Damage(int damage)
    {
        Debug.LogWarning("P" + (playerIndex + 1) + " DAMAGED (" + damage + ")!");
        health -= damage;

		if (health <= 0)
			Die ();
		else {
            FXUtil.CreateVisualEffect("PlayerHit").SetPosition(transform.position);
            FXUtil.CreateSoundEffect("Hit");

            ScriptsPointer.HUDController.UpdatePlayerHealthUI (playerIndex, health);
		}
    }

    public void Die()
    {
		if (isLocal)
			GetComponent<PlayerMovementController> ().BroadcastDied ();

        Debug.LogWarning("P" + (playerIndex + 1) + " DIED!");

        FXUtil.CreateVisualEffect("PlayerCrash").SetPosition(transform.position);
        FXUtil.CreateSoundEffect("Crash");

        health = 0;
        dead = true;
        SetVisible(false);
        SetControlActive(false);
        GetTeam().playersAlive--;
        
        ScriptsPointer.HUDController.UpdatePlayerHealthUI(playerIndex, 0);
        if (GetTeam().playersAlive < 1)
        {
            if (GetTeam().teamRole == Team.Role.ATK)
                ScriptsPointer.GameController.Score(ScriptsPointer.MatchData.GetTeamByRole(Team.Role.DEF).teamIndex);
            else
                ScriptsPointer.GameController.Score(ScriptsPointer.MatchData.GetTeamByRole(Team.Role.ATK).teamIndex);
        }
    }
}