﻿using UnityEngine;
using System.Collections;
using GameSparks.RT;

public class OpponentRTDataListener : MonoBehaviour {

	public PlayerBaseInfo baseInfo;
    public float speed;
	public Vector2 moveDir;
	public Vector2 velocity;
	public bool accelerating;

	private Vector2 newPos;
	private float newRot;

	private bool bot;

	void Start()
	{
		GetComponent<PolygonCollider2D> ().sharedMaterial = null;
		Destroy (GetComponent<Rigidbody2D> ());
		newPos = transform.position;
		newRot = transform.eulerAngles.z;
	}

	public void Reset()
	{
		baseInfo.Reset ();
		newPos = transform.position;
		newRot = transform.eulerAngles.z;
	}

	void Update()
	{
		if (baseInfo.controlActive) {
			float updateRate = Time.deltaTime / baseInfo.syncUpdateRate;
			transform.position = Vector2.Lerp (transform.position, newPos, updateRate);
			transform.eulerAngles = new Vector3 (0, 0, Mathf.LerpAngle (transform.eulerAngles.z, newRot, updateRate));

			if (accelerating) {
				if (baseInfo.smokeTrail.isStopped)
					baseInfo.smokeTrail.Play ();
			}
			else {
				if (baseInfo.smokeTrail.isPlaying)
					baseInfo.smokeTrail.Stop ();
			}
		}
		else {
			if (baseInfo.smokeTrail.isPlaying)
				baseInfo.smokeTrail.Stop ();
		}

		moveDir = transform.TransformDirection (Vector2.up);
	}

	public void UpdatePlayerMovement(Vector2 _prevPos, Vector2 _velocity, float _rotation)
	{
		newPos = _prevPos + _velocity;
		newRot = _rotation;
		velocity = _velocity;
        speed = _velocity.magnitude;
	}

	public void UpdatePlayerLanded(int playerIndex, int teamIndex)
	{
        baseInfo.SetControlActive(false);
        baseInfo.landed = true;

		Platform platform = GameObject.Find ("Platform").GetComponent<Platform> ();
        platform.Capture(playerIndex, teamIndex);
	}

	public void UpdatePlayerCollided(int damage)
	{
        baseInfo.Damage(damage);
	}

	public void UpdatePlayerDied()
	{
		baseInfo.Die ();
	}

    public void UpdatePlayerWarped(Vector2 pos)
    {
        transform.position = pos;
        newPos = transform.position;
    }

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) {
            var opp = other.gameObject.GetComponent<OpponentRTDataListener>();
            if (opp != null)
            {
                if (speed > opp.speed)
                {
                    FXUtil.CreateVisualEffect("PlayerHit").SetPosition(transform.position);
                    FXUtil.CreateSoundEffect("Hit");
                }
            }
		}
	}
}
