﻿using UnityEngine;
using System.Collections;
using GameSparks.Core;
using System.Collections.Generic;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour {

	private static MessageManager instance;

	public GameMessage[] gameMessages;
	private Dictionary<string, GameMessage> messageDict;
	private GameMessage currentMessage;

	public static void Unload() { instance = null; }
	void Awake() {
		instance = this;
		messageDict = new Dictionary<string, GameMessage> ();
		foreach (var gMsg in gameMessages)
			messageDict.Add (gMsg.label, gMsg);
	}

	public static void ShowMessage(string msg)
	{
		if (IsLoaded) {
			instance.currentMessage = instance.messageDict [msg];
			instance.GetComponent<Animator>().Play (instance.currentMessage.label, 0, 0f);
			instance.currentMessage.onPlay.Invoke ();
		}
	}

	private static bool IsLoaded {
		get {
			if (instance != null)
				return true;
			else {
				Debug.LogError ("MessageManager| No manager instance is loaded.");
				return false;
			}
		}
	}

	public void SetRoundStartText()
	{
		currentMessage.uiRef.Find ("Round").GetComponent<Text>().text = "ROUND " + ScriptsPointer.MatchData.GetRoundNumber();
	}
}

[System.Serializable]
public class GameMessage
{
	public string label;
	public Transform uiRef;
	public UnityEvent onPlay;
}