﻿using UnityEngine;
using System.Collections;

public class MatchData : MonoBehaviour {
    
	/*public struct LocalPlayerInfo
	{
		public int peerId;
		public string displayName;
		public int teamIndex;
	}*/

    public static MatchData CurrentMatch = null;
	public static MatchData Get() { return CurrentMatch; }
    //public LocalPlayerInfo localPlayerInfo;
    
    public int roundTime;
    public int GetRoundTime() { return roundTime; }

    private int timePassed = 0;
    public int GetTimePassed() { return timePassed; }
    public int GetTimeLeft() { return (roundTime - timePassed); }
	public void TickTime() { timePassed++; }

    private int roundNumber = 0;
	public int GetRoundNumber() { return roundNumber; }

	private Team[] team = new Team[2];
	public Team GetTeam(int teamIndex) { return team [teamIndex]; }
    public Team GetTeamByRole(Team.Role _role) {
        foreach (var t in team)
            if (t.teamRole == _role) return t;
        return null;
    }

    public bool IsMatchpoint() { return (team[0].score>1 && team[1].score>1); }

    void Awake() {
        roundNumber = 1;
        CurrentMatch = this;
    }

	public void SetupTeams()
	{
		team [0] = new Team().Define (0, Team.Role.ATK, GameData.TeamColor[0]);
		team [1] = new Team().Define (1, Team.Role.DEF, GameData.TeamColor[1]);
	}

	public void SwitchTeamRoles()
	{
		foreach (var t in team) {
			t.SwitchRole ();
		}
	}
	
	public void ResetTime()
	{
		timePassed = 0;
		ScriptsPointer.HUDController.UpdateTimeUI ();
	}

	public void ResetTeams()
	{
		team [0].playersAlive = team [0].totalPlayersAmount;
		team [1].playersAlive = team [1].totalPlayersAmount;
	}

    public void NewRound()
    {
        Debug.Log("MatchData| Resetting round data.");
		roundNumber++;
		ResetTeams ();
		SwitchTeamRoles ();
		ResetTime ();
    }
}

public class Team
{
	public enum Role { ATK=0, DEF=1 };
	public static string[] roleLabel = { "ATTACK", "DEFEND" };

	public int teamIndex;
	public Role teamRole;
	public Color teamColor;
	public int score;
    public int playersAlive;
	public int totalPlayersAmount;

	public Team Define(int index, Role role, Color color)
	{
		teamIndex = index;
		teamRole = role;
		teamColor = color;
		return this;
	}
	public void SwitchRole()
	{
		if (teamRole == Role.ATK)
			teamRole = Role.DEF;
		else
			teamRole = Role.ATK;
	}

    public int RoleIndex
    {
        get
        {
            if (teamRole == Role.ATK)
                return 0;
            else return 1;
        }
    }
}