﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GSNetworking.ApiManagers;

public class HUDController : MonoBehaviour
{

    public Text uiTimer;
    public Text uiRole;
    public Text uiMatchpoint;

    public Transform[] teamScoreUI = new Transform[2];
    public Transform[] playerFrameUI = new Transform[8];

    public Text uiMsgRole;
    public Text uiMsgRound;

    void Start()
    {
        UpdateTimeUI();
        SetupPlayerFrames();
    }

    public void SetupPlayerFrames()
    {
        for (int index = 0; index < playerFrameUI.Length; index++)
        {
            int peer = (index + 1);
            if (peer > RealtimeService.SessionInfo.GetPlayers().Count)
                HidePlayerFrameUI(index);
            else
            {
                if (RealtimeService.Api.PeerId != peer)
                    SetOpponentFrameUITransparent(index);
                playerFrameUI[index].Find("Dead").GetComponent<Image>().enabled = false;
                UpdatePlayerNameUI(index);
            }
        }
    }

    public void HidePlayerFrameUI(int pIndex)
    {
        playerFrameUI[pIndex].gameObject.SetActive(false);
    }

    public void SetOpponentFrameUITransparent(int pIndex)
    {
        Color _color = playerFrameUI[pIndex].GetComponent<Image>().color;
        _color.a = 0.66f;
        playerFrameUI[pIndex].GetComponent<Image>().color = _color;

        Dictionary<string, string> ObjUiComponentType = new Dictionary<string, string>();
        System.Func<string, Transform> GetHUDElement = (elementName) => { return playerFrameUI[pIndex].Find(elementName); };

        ObjUiComponentType.Add("Name", "Text");
        ObjUiComponentType.Add("Health", "Text");
        ObjUiComponentType.Add("Dead", "Image");

        Color newCol = new Color(1, 1, 1, 0.66f);
        string[] arr = { "Name", "Health", "Dead" };

        for (int i = 0; i < ObjUiComponentType.Count; i++)
        {
            var UIelement = GetHUDElement(arr[i]);
            if (ObjUiComponentType[arr[i]] == "Text")
                UIelement.GetComponent<Text>().color = newCol;
            else if (ObjUiComponentType[arr[i]] == "Image")
                UIelement.GetComponent<Image>().color = newCol;
        }
    }

    public void ResetAndUpdateAllUI()
    {
        UpdateTimeUI();
        UpdateRoleUI();
        foreach (var plr in ScriptsPointer.GameController.playerList)
            UpdatePlayerHealthUI(plr.playerIndex, 100);
        foreach (var plrUi in playerFrameUI)
        {
            plrUi.Find("Dead").GetComponent<Image>().enabled = false;
            plrUi.Find("Health").GetComponent<Text>().enabled = true;
        }
    }

    public void SetTextColors()
    {
        var playerInfo = ScriptsPointer.GameController.playerList[(int)RealtimeService.Api.PeerId - 1];
        uiMsgRole.color = GameData.TeamColor[playerInfo.teamIndex];
        uiRole.color = GameData.TeamColor[playerInfo.teamIndex];
    }

    public void UpdateMatchpointUI()
    {
        int team1Score = ScriptsPointer.MatchData.GetTeam(0).score;
        int team2Score = ScriptsPointer.MatchData.GetTeam(1).score;

        bool isMatchpoint = (team1Score == 2 || team2Score == 2);

        uiMatchpoint.enabled = isMatchpoint;
    }

    public void UpdateTimeUI()
    {
        MatchData _matchData = ScriptsPointer.MatchData;

        int timeLeft = _matchData.GetRoundTime() - _matchData.GetTimePassed();
        System.TimeSpan tsTime = new System.TimeSpan(0, 0, timeLeft);
        uiTimer.text = string.Format("{0:00}:{1:00}", tsTime.Minutes, tsTime.Seconds);

        if (timeLeft <= 10)
            uiTimer.color = Color.red;
        else uiTimer.color = Color.gray;
    }

    public void UpdateScoreUI(int teamIndex, int newScore)
    {
        Transform newScoreUI = teamScoreUI[teamIndex].Find("Score" + newScore + "/Fill");

        if (newScoreUI != null)
            newScoreUI.GetComponent<Image>().enabled = true;
    }

    public void UpdatePlayerHealthUI(int playerIndex, int newHealth)
    {
        Transform plrHealthUI = playerFrameUI[playerIndex].Find("Health");
        if (newHealth > 0)
        {
            if (plrHealthUI != null)
                plrHealthUI.GetComponent<Text>().text = newHealth.ToString();
        }
        else
        {
            if (plrHealthUI != null)
                plrHealthUI.GetComponent<Text>().enabled = false;

            Transform plrSkullUI = playerFrameUI[playerIndex].Find("Dead");
            if (plrSkullUI != null)
                plrSkullUI.GetComponent<Image>().enabled = true;
        }
    }

    public void UpdatePlayerNameUI(int playerIndex)
    {
        string pName = RealtimeService.SessionInfo.GetPlayers()[playerIndex].DisplayName;
        playerFrameUI[playerIndex].Find("Name").GetComponent<Text>().text = pName;
    }

    public void UpdateUIRoleMessage(Team _plrTeamInfo)
    {
        if (_plrTeamInfo.teamRole == Team.Role.ATK)
            uiMsgRole.text = "ATTACK";
        else uiMsgRole.text = "DEFEND";
    }

    public void UpdateRoleUI()
    {
        var playerInfo = ScriptsPointer.GameController.playerList[(int)RealtimeService.Api.PeerId - 1];
        uiRole.text = Team.roleLabel[(int)playerInfo.GetTeam().RoleIndex];
    }
}
