﻿using UnityEngine;

namespace Data
{
    public class PersistentPlayerData
    {
        public static string DevicePassword;

        public static string GetDevicePassword()
        {
            if (PlayerPrefs.HasKey("DevicePassword"))
                DevicePassword = PlayerPrefs.GetString("DevicePassword");
            else DevicePassword = SetDevicePassword(SystemInfo.deviceUniqueIdentifier);

            return DevicePassword;
        }

        public static string SetDevicePassword(string pw) {
            PlayerPrefs.SetString("DevicePassword", pw);
            PlayerPrefs.Save();

            return PlayerPrefs.GetString("DevicePassword");
        }
    }
}
