﻿using UnityEngine;
using System.Collections;

public class CharacterDisplayInfo : MonoBehaviour {

    public Color[] skinColors;
    public static Color[] SkinColors;

    public Sprite[] hatsSprites;
    public static Sprite[] HatsSprites;

    public Sprite[] eyesSprites;
    public static Sprite[] EyesSprites;

    public Sprite[] clothesSprites;
    public static Sprite[] ClothesSprites;

    void Awake()
    {
        ClothesSprites = clothesSprites;
        EyesSprites = eyesSprites;
        HatsSprites = hatsSprites;
        SkinColors = skinColors;
    }
}
