﻿using UnityEngine;
using System.Collections;

public class GameData : MonoBehaviour {

	public float playerAccelerationForce;
	public static float PlayerAccelerationForce;

	public float playerRotateSpeed;
	public static float PlayerRotateSpeed;

	public float playerSyncUpdateRate;
	public static float PlayerSyncUpdateRate;

    public float playerSpeedMaxCrash;
    public static float PlayerSpeedMaxCrash;

    public float playerSpeedMinDamage;
    public static float PlayerSpeedMinDamage;

	public Color[] teamColor = new Color[2];
	public static Color[] TeamColor = new Color[2];

	public Color[] characterSkinColor = new Color[8];
	public static Color[] CharacterSkinColor = new Color[8];

	public static string OS;

	void Awake()
	{
		PlayerAccelerationForce = playerAccelerationForce;
		PlayerRotateSpeed = playerRotateSpeed;
		PlayerSyncUpdateRate = playerSyncUpdateRate;
        PlayerSpeedMaxCrash = playerSpeedMaxCrash;
        PlayerSpeedMinDamage = playerSpeedMinDamage;
        TeamColor = teamColor;
		CharacterSkinColor = characterSkinColor;

		OS = GetOS();
		Debug.Log ("GameData| System OS string: '" + OS + "'.");
	}

	private static string GetOS()
	{
		string systemOs = SystemInfo.operatingSystem.ToUpper();
		if (systemOs.Contains ("WIN"))
			return "WIN";
		else if (systemOs.Contains ("MAC"))
			return "MAC";
		else
			return "";
	}
}
