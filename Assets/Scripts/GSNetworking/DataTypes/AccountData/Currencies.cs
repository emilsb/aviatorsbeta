﻿using GameSparks.Core;
using System.Collections.Generic;

namespace GSNetworking.DataTypes.AccountData
{
    public class Currencies
    {
        public List<int> CurrencyList;
        public List<GSData> ReservedCurrency;

        public Currencies(GSData data)
        {
            CurrencyList = new List<int>();
            ReservedCurrency = new List<GSData>();

            // Loop throudh Currency indices and add them to list
            for (int cIndex = 1; cIndex <= 6; cIndex++)
                CurrencyList.Add((int)data.GetLong("Currency" + cIndex).Value);

            // Loop throudh Reserved Currency indices and add them to list
            for (int rcIndex = 1; rcIndex <= 6; rcIndex++)
                ReservedCurrency.Add(data.GetGSData("ReservedCurrency" + rcIndex));
        }
    }
}
