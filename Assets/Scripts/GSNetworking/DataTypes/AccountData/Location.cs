﻿using GameSparks.Api.Responses;
using GameSparks.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GSNetworking.DataTypes.AccountData
{
    public struct Location {
        /// <summary> Coordinates data container.\nFields:   Latitide (double),   Longditude (double) </summary>
        public struct Coordinates {
            public double latitide; public double longditude;

            public Coordinates(double lat, double lon)
            {
                latitide = lat;
                longditude = lon;
            }
        }


        // Location data fields
        public string city, country;
        public Coordinates coordinates;


        /// <summary>
        /// Constructs a new Location object and converts the info from the provided GSData object.
        /// </summary>
        /// <param name="data">A GSData object containing the City, Country, Latitide and Longditute KeyValuePairs.</param>
        public Location(GSData data)
        {
            coordinates = new Coordinates();
            city = data.GetString("City");
            country = data.GetString("Country");

            coordinates.latitide = data.GetDouble("Latitide").Value;
            coordinates.longditude = data.GetDouble("Longditute").Value;
        }

        /// <summary>
        /// Constructs a new Location object and converts data provided with the Gamesparks.Api.Responses.AccountDetailsResponse._Location data
        /// </summary>
        /// <param name="data">The location data to convert</param>
        public Location(AccountDetailsResponse._Location data)
        {
            city = data.City;
            country = data.Country;
            coordinates = new Coordinates(
                data.Latitide.Value,
                data.Longditute.Value
            );
        }
    }
}
