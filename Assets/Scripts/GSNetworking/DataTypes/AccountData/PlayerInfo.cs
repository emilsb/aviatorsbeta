﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Responses;
using GSNetworking.DataTypes.RealtimeData;
using GameSparks.Api.Messages;
using GSNetworking.ParametersData;

namespace GSNetworking.DataTypes.AccountData
{
	public class PlayerInfo
	{
		public string AuthToken;
		public string DisplayName;
		public string UserId;
		public PlayerStatsInfo StatsData;
		public GSData CustomData;

		public PlayerInfo (RegistrationResponse data)
		{
			AuthToken = data.AuthToken;
			DisplayName = data.DisplayName;
			UserId = data.UserId;
			CustomData = data.ScriptData;
		}

		public PlayerInfo (MatchFoundMessage._Participant _part)
		{
			AuthToken = string.Empty;
			DisplayName = _part.DisplayName;
			UserId = _part.Id;
			CustomData = _part.ScriptData;
			StatsData = LinkStats(_part);
		}

		public PlayerStatsInfo LinkStats(MatchFoundMessage._Participant _part)
		{
			ParticipantData data;
			data.achievements = _part.Achievements;
			data.externalData = _part.ExternalIds;
			data.isOnline = _part.Online.Value;
			data.peerId = _part.PeerId.Value;
			data.virtualGoods = _part.VirtualGoods;
			data.customData = _part.ScriptData;

			return (new PlayerStatsInfo(data));
		}
	}
}

