﻿using System;
using System.Collections.Generic;
using GSNetworking.ParametersData;
using GSNetworking.DataTypes.AccountData;
using GSNetworking.DataTypes.RealtimeData;

namespace GSNetworking.DataTypes
{
	public class UserData
	{
		public static AuthCredentials AuthCredentials;
        public static PlayerCharacterData CharacterData;
		public static PlayerInfo PlayerInfo;

		public UserData(PlayerInfo _plrInfo) { PlayerInfo = _plrInfo; }
	}
}