﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameSparks.Api.Messages;
using GameSparks.Core;

namespace GSNetworking.DataTypes.RealtimeData
{
    public class PlayerStatsInfo
    {
		public List<string> Achievements;
		public GSData ExternalData;
		public bool IsOnline;
		public int PeerId;
		public List<string> VirtualGoods;
		public GSData CustomData;

		public PlayerStatsInfo(ParametersData.ParticipantData _data)
        {
			Achievements = _data.achievements;
			ExternalData = _data.externalData;
			IsOnline = _data.isOnline;
			PeerId = _data.peerId;
			VirtualGoods = _data.virtualGoods;
			CustomData = _data.customData;
        }
    }
}
