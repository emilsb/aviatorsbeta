﻿using GameSparks.Api.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSNetworking.DataTypes.AccountData;

namespace GSNetworking.DataTypes.RealtimeData
{
    public class SessionInfo
    {
        private string hostURL;
        public string GetHostURL() { return this.hostURL; }
        private string acccessToken;
        public string GetAccessToken() { return this.acccessToken; }
        private int portID;
        public int GetPortID() { return this.portID; }
        private string matchID;
        public string GetMatchID() { return this.matchID; }
		public bool IsCreator;

		private List<PlayerInfo> playersInfo = new List<PlayerInfo>();
		public List<PlayerInfo> GetPlayers() { return playersInfo; }

        public SessionInfo(MatchFoundMessage _message)
        {
            portID = (int)_message.Port;
            hostURL = _message.Host;
            acccessToken = _message.AccessToken;
            matchID = _message.MatchId;

			PlayerInfo[] temp = new PlayerInfo[_message.Participants.Count ()];
            foreach (MatchFoundMessage._Participant p in _message.Participants)
            {
				if (p.PeerId == 1)
					IsCreator = true;

				PlayerInfo plrInfo = new PlayerInfo(p);
				plrInfo.StatsData = plrInfo.LinkStats(p);

				int pIndex = ((int)p.PeerId - 1);
				temp[pIndex] = plrInfo;
            }
			playersInfo.AddRange(temp);
        }
    }
}
