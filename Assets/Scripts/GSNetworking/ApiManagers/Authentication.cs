﻿using UnityEngine;
using System.Collections;
using GameSparks.Api.Responses;
using GameSparks.Api.Requests;
using GameSparks.Core;
using GSNetworking.DataTypes.AccountData;
using GSNetworking.ParametersData;
using GSNetworking.DataTypes;

namespace GSNetworking.ApiManagers {

    public class Authentication {
        public static bool IsLoggedIn;

        public delegate void LoginCallback();
        public delegate void RegistrationCallback(AuthCredentials _creds, bool newUser);
        public delegate void AccountUpdatedCallback(AccountDetailsResponse _response);

        /*public static void LoginWithDevice(string _dipslayName, LoginCallback _onLoginSuccess, LoginCallback _onLoginFail)
        {
            new DeviceAuthenticationRequest()
            .Send((response) => {
                if (response.HasErrors)
                {
                    Debug.LogError("GSAuthentication| DeviceAuthentication failed. Error message: " + response.Errors.JSON);
                    IsLoggedIn = false;
                    _onLoginFail(response);
                }

                else
                {
                    Debug.Log("GSAuthentication| DeviceAuthentication succeeded. Setting DisplayName.");
                    IsLoggedIn = true;

                    AccountDetailsInfo _newDetails = new AccountDetailsInfo(_dipslayName, false);
                    Debug.Log("GSAuthentication| Updated displayName to: " + UserAccount.UpdateAccountDetails(_newDetails).DisplayName);
                }

            });
        }*/

        public static void RegisterAccount(AuthCredentials accCreds, RegistrationCallback registrationSuccess, RegistrationCallback registrationFailed)
        {
            Debug.Log("GSNetworking.ApiManagers.Authentication | Using password (" + accCreds.password + ") for registration request.");

            new RegistrationRequest()
            .SetDisplayName(accCreds.username)
            .SetUserName(accCreds.username)
            .SetPassword(accCreds.password)
            .Send((regResponse) => {
                
				PlayerInfo newPlrInfo = new PlayerInfo(regResponse);
                if (!regResponse.HasErrors)
                {
                    Debug.Log("GSNetworking.ApiManagers.Authentication | Registered new user.");
					UserData.PlayerInfo = newPlrInfo;
					UserData.AuthCredentials = accCreds;
                    registrationSuccess(accCreds, true);
                }

                else // If registration fails, check if user already exists
                {
                    bool newUser = (bool)regResponse.NewPlayer;
                    // If player already exists, attempt login instead
					if (!newUser)
					{
						UserData.PlayerInfo = newPlrInfo;
						UserData.AuthCredentials = accCreds;
                    	Debug.Log("GSNetworking.ApiManagers.Authentication | Existing user found, attempting login instead.");
					}
                    else // if there is another error, then the registration must have failed
                        Debug.LogError("GSNetworking.ApiManagers.Authentication | Account not found. Registration failed.\n\tERROR - " + regResponse.Errors.JSON);
					
                    registrationFailed(accCreds, newUser);
                }
            });
        }

        public static void LoginWithAccountInfo(AuthCredentials credentials, LoginCallback loginSuccess, LoginCallback loginFailed)
        {
            new AuthenticationRequest()
            .SetUserName(credentials.username)
            .SetPassword(credentials.password)
            .Send((loginResponse) =>
            {
                if (!loginResponse.HasErrors)
                {
                    Debug.Log("GSNetworking.ApiManagers.Authentication| Login Success!");
					IsLoggedIn = true;
                    loginSuccess();
                }
                else
                {
                    Debug.LogError("GSNetworking.ApiManagers.Authentication| Login failed.\n\tERROR: [ " + loginResponse.Errors.JSON + " ] ");
                    loginFailed();
                }
            });
        }

        public static void LoginWithSteam(string _displayName, RegistrationCallback onRegistered, LoginCallback onLoggedIn)
        {

        }
    }
}
