﻿using UnityEngine;
using System.Collections;
using GameSparks.Api.Requests;
using GameSparks.Core;

namespace GSNetworking.ApiManagers {
	
	public class Analytics {

		public static void RecordTeamRoleWin(string roleLabel)
		{
			roleLabel = roleLabel.ToLower () + "ers";
			string statKey = "win_count_" + roleLabel;

			new LogEventRequest ()
				.SetEventKey ("RecordStatistic")
				.SetEventAttribute("TYPE", statKey)
				.Send ((response) => {
					
				}
			);
		}

	}
}