﻿using UnityEngine;
using System.Collections;
using GameSparks.Api.Responses;
using GameSparks.Api.Requests;
using GSNetworking;
using GSNetworking.ParametersData;
using GSNetworking.DataTypes;
using System.Collections.Generic;

namespace GSNetworking.ApiManagers
{
    public delegate void AccountUpdateDelegate(AccountDetailsInfo _info);
    public delegate void CharacterUpdateDelegate(CharacterEquipmentInfo _info);
    public delegate void RetrievedAccountDetailsDelegate(AccountDetailsResponse _response);

    public class UserAccount
    {
        // CALLBACK DELEGATES
        private static AccountUpdateDelegate AccountUpdated;
        private static CharacterUpdateDelegate CharacterUpdated;
        private static RetrievedAccountDetailsDelegate RetrievedAccountDetails;


        // FUNCTIONS
        public static void UpdateAccountDetails(AccountDetailsInfo _newDetails, AccountUpdateDelegate _callback = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.UpdateAccountDetails | -- Sending ChangeUserDetailsRequest");
            AccountUpdated += OnAccountUpdated;

            var request = new ChangeUserDetailsRequest();
            if (_newDetails.displayName != string.Empty) request = request.SetDisplayName(_newDetails.displayName);
            if (_newDetails.username != string.Empty) request = request.SetUserName(_newDetails.username);
            if (_newDetails.newPassword != string.Empty) request = request.SetNewPassword(_newDetails.newPassword);
            if (_newDetails.currentPassword != string.Empty) request = request.SetOldPassword(_newDetails.currentPassword);
            request.Send((response) => {
                if (!response.HasErrors) {
                    Debug.Log("GSNetworking.ApiManagers.UserAccount.UpdateAccountDetails | -- Updated account details!");
                    if (_newDetails.displayName != string.Empty) Debug.Log("\tUpdated DisplayName: " + _newDetails.displayName);
                    if (_newDetails.username != string.Empty) Debug.Log("\tUpdated Username: " + _newDetails.username);
                    if (_newDetails.newPassword != string.Empty) Debug.Log("\tUpdated Password: " + _newDetails.newPassword);
                }
                else { Debug.LogError("GSNetworking.ApiManagers.UserAccount.UpdateAccountDetails | -- Error updating account.\n\tERROR_DATA: [ " + response.Errors.JSON + " ]."); }
                
                if (_callback != null) AccountUpdated += _callback;
                AccountUpdated(_newDetails);
            });
        }

        public static void UpdateCharacterData(CharacterEquipmentInfo _newDetails, CharacterUpdateDelegate _callback = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.UpdateCharacterData | -- Sending LogEventRequest.UploadPlayerCharacterData");
            CharacterUpdated += OnCharacterUpdated;

            var request = new LogEventRequest().SetEventKey("UploadPlayerCharacterData");
            if (_newDetails.hatId >= 0) request.SetEventAttribute("HAT_ID", _newDetails.hatId);
            else _newDetails.hatId = UserData.CharacterData.HatID;

            if (_newDetails.eyesId >= 0) request.SetEventAttribute("EYES_ID", _newDetails.eyesId);
            else _newDetails.eyesId = UserData.CharacterData.EyesID;

            if (_newDetails.clothesId >= 0) request.SetEventAttribute("CLOTHES_ID", _newDetails.clothesId);
            else _newDetails.clothesId = UserData.CharacterData.ClothesID;

            if (_newDetails.skinId >= 0) request.SetEventAttribute("SKIN_ID", _newDetails.skinId);
            else _newDetails.skinId = UserData.CharacterData.SkinID;

            request.Send((response) => {
                if (!response.HasErrors)
                {
                    Debug.Log("GSNetworking.ApiManagers.UserAccount.UpdateCharacterData | -- Uploaded character data to server!");
                    if (_callback != null) CharacterUpdated += _callback;
                    CharacterUpdated(_newDetails);
                }
                else
                    Debug.LogError("GSNetworking.ApiManagers.UserAccount.UpdateCharacterData | -- Error uploading data.\n\tERROR_DATA: [ " + response.Errors.JSON + " ].");
            });
        }

        public static AccountDetailsResponse RetrieveAccountDetails(RetrievedAccountDetailsDelegate _callback = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.RetrieveAccountDetails | -- Sending AccountDetailsRequest");
            RetrievedAccountDetails += OnAccountInfoRetrieved;

            AccountDetailsResponse retrievedDetails = null;
            new AccountDetailsRequest().Send((response) => {
                if (!response.HasErrors)
                {
                    Debug.Log("GSNetworking.ApiManagers.UserAccount.RetrieveAccountDetails | -- Retrieved account details!");
                    retrievedDetails = new AccountDetailsResponse(response.BaseData);
                    if (_callback != null) RetrievedAccountDetails += _callback;
                    RetrievedAccountDetails.Invoke(retrievedDetails);
                }
                else
                    Debug.LogError("GSNetworking.ApiManagers.UserAccount.RetrieveAccountDetails | -- Error retrieving account details.\n\tERROR_DATA: [ " + response.Errors.JSON + " ].");

            });

            // Wait until a response has been recieved
            while (true) { if (retrievedDetails != null) break; }
            Debug.Log("GSNetworking.ApiManagers.UserAccount.RetrieveAccountDetails | -- Returning AccountDetailsResponse to caller.");
            return retrievedDetails;
        }


        // CALLBACKS
        private static void OnAccountUpdated(AccountDetailsInfo _data = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.OnAccountUpdated callback triggered!");
        }

        private static void OnCharacterUpdated(CharacterEquipmentInfo _newData = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.OnCharacterUpdated callback triggered!");
            if (_newData != null) {
                UserData.CharacterData = new PlayerCharacterData(
                    _newData.hatId,
                    _newData.eyesId,
                    _newData.clothesId,
                    _newData.skinId
                );
            }

        }

        private static void OnAccountInfoRetrieved(AccountDetailsResponse _response = null) {
            Debug.Log("GSNetworking.ApiManagers.UserAccount.OnRetrievedAccountDetails callback triggered!");
        }


        //####################################//
        //     Private shared methods         //
        //####################################//
    }
}

