﻿using UnityEngine;
using System.Collections;

namespace GSNetworking.ApiManagers
{
    public class Matchmaking
    {
		public static string findMatchType;
        public static System.Action MatchmakingCanceledMessage;

        public static void FindMatch(string gsShortCode)
        {
			findMatchType = gsShortCode;
            new GameSparks.Api.Requests.MatchmakingRequest()
                .SetMatchShortCode(gsShortCode)
                .SetSkill(0)
                .Send((response) => {
                    if (response.HasErrors)
                        Debug.LogError("GSMatchManager| MatchMaking Error \n" + response.Errors.JSON);
                });
        }

        public static void CancelMatchmaking(string gsShortCode, System.Action callback = null)
        {
            MatchmakingCanceledMessage += OnMatchmakingCanceled;
            if (callback != null) MatchmakingCanceledMessage += callback;

            Debug.Log("GS.Api.Matchmaking| Canceling matchmaking.");

            new GameSparks.Api.Requests.LogEventRequest()
                .SetEventKey("CancelMatchmaking")
                .SetEventAttribute("SHORT_CODE", gsShortCode)
                .Send((response) => {
                    if (!response.HasErrors)
                    {
                        Debug.Log("GS.Api.Matchmaking| Canceled matchmaking successfully.");
                        MatchmakingCanceledMessage();
                    }
                    else
                        Debug.LogError("GS.Api.Matchmaking| CanceMatchmaking failed. ERROR: " + response.Errors.JSON);
                });
        }

        public static void OnMatchmakingCanceled()
        {
            Debug.Log("GS.Api.Matchmaking| OnMatchmakingCanceled message callback triggered.");
        }
    }
}

