﻿using GameSparks.Api.Messages;
using GameSparks.Api.Responses;
using GameSparks.Core;
using GameSparks.RT;
using GSNetworking.DataTypes.RealtimeData;
using System.Collections.Generic;
using UnityEngine;

namespace GSNetworking.ApiManagers
{
    public class RealtimeService
    {
        public static GameSparksRTUnity Api;
        public static SessionInfo SessionInfo;

        public static bool IsReady;

        public static void StartNewSession(SessionInfo _info)
        {
            Debug.Log("GSRealtime| Creating New RT Session Instance...");
            SessionInfo = _info;
            Api = new GameObject("GSRealtimeSession").AddComponent<GameSparksRTUnity>();

            GSRequestData mockedResponse = new GSRequestData()
                .AddNumber("port", (double)_info.GetPortID())
                .AddString("host", _info.GetHostURL())
                .AddString("accessToken", _info.GetAccessToken())
            ;

            FindMatchResponse response = new FindMatchResponse(mockedResponse);

            Api.Configure(response,
                (peerId) => { OnPlayerConnected(peerId); },
                (peerId) => { OnPlayerDisconnected(peerId); },
                (ready) => { OnRTReady(ready); },
                (packet) => { OnPacketReceived(packet); });
            Api.Connect();

        }

        private static void OnPlayerConnected(int _peerId)
        {
            Debug.Log("GSRealtime| Player Connected, " + _peerId);
        }

        private static void OnPlayerDisconnected(int _peerId)
        {
            Debug.Log("GSRealtime| Player Disconnected, " + _peerId);
        }

        private static void OnPacketReceived(RTPacket _packet)
        {
            switch (_packet.OpCode)
            {

                case 20:
                    GSRTManager.GetInstance().UpdateOpponentTransforms(_packet);
                    break;

                case 21:
                    GSRTManager.GetInstance().RegisterOpponentCollision(_packet);
                    break;

                case 22:
                    GSRTManager.GetInstance().RegisterOpponentLanded(_packet);
                    break;

                case 23:
                    GSRTManager.GetInstance().RegisterOpponentDied(_packet);
                    break;

                case 24:
                    GSRTManager.GetInstance().RegisterOpponentWarped(_packet);
                    break;
            }
        }

        private static void OnRTReady(bool _isReady)
        {
            IsReady = _isReady;
            if (_isReady)
            {
                Debug.Log("GSRealtime| Session Connected!");
            }
        }
    }
}
