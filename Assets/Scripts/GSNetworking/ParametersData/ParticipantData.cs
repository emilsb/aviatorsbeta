﻿using System;
using System.Collections.Generic;
using GameSparks.Core;

namespace GSNetworking.ParametersData
{
	public struct ParticipantData
	{
		public List<string> achievements;
		public GSData externalData;
		public bool isOnline;
		public int peerId;
		public List<string> virtualGoods;
		public GSData customData;
	}
}

