﻿using UnityEngine;
using System.Collections;

namespace GSNetworking.ParametersData {

    public class AccountDetailsInfo
    {
        public string displayName = string.Empty;
        public string username = string.Empty;
        public string currentPassword = string.Empty;
        public string newPassword = string.Empty;

        // Set new DISPLAYNAME (Without updating USERNAME)
        public AccountDetailsInfo(string _displayName) { displayName = _displayName; }

        // Set new USERNAME (Also updates DISPLAYNAME if checked)
        public AccountDetailsInfo(string _username, bool _updateDisplayName) {
            username = _username;
            if (_updateDisplayName)
                displayName = _username;
        }

        // Set new USERNAME and PASSWORD (Also updates DISPLAYNAME if checked) 
        public AccountDetailsInfo(string _username, bool _updateDisplayName, string _currentPassword, string _newPassword)
        {
            username = _username;
            currentPassword = _currentPassword;
            newPassword = _newPassword;

            if (_updateDisplayName)
                displayName = _username;
        }
    }

}
