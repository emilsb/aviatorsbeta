﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;


namespace GSNetworking
{
    public class ApiInitializer : MonoBehaviour
    {

		private static ApiInitializer instance = null;
        public static bool IsConnected;

        void Awake()
        {
            IsConnected = false;
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
                Destroy(this.gameObject);
        }

        void Start()
        {
            GameSparks.Core.GS.GameSparksAvailable += OnConnected;
        }

        private void OnConnected(bool connected)
        {
            if (connected)
                Debug.Log("GS.Initializer| Connected to server!");
            else Debug.Log("GS.Initializer| Disconnected from server!");

            IsConnected = connected;
        }
    }
}