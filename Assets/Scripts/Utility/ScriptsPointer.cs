﻿using UnityEngine;
using System.Collections;

public class ScriptsPointer : MonoBehaviour {

    public GameController gameController;
	public static GameController GameController;

    public HUDController hudController;
	public static HUDController HUDController;

    public MatchData matchData;
	public static MatchData MatchData;

    private static ScriptsPointer instance = null;
    public static ScriptsPointer Instance() { return instance; }

	void Awake()
    {
        instance = this;
		GameController = gameController;
		HUDController = hudController;
		MatchData = matchData;
    }
	
	void Update () {
		if (GameController != gameController)
			GameController = gameController;

		if (HUDController != hudController)
			HUDController = hudController;

		if (MatchData != matchData)
			MatchData = matchData;
	}
}
