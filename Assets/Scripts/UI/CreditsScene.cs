﻿using UnityEngine;
using System.Collections;
using Extensions.UnityEngine;
using System.Collections.Generic;

public class CreditsScene : MonoBehaviour {

    enum CreditPart { Millenway, EclipseStudios };

    public Animator credFrame501;
    public Animator credFrameES;
	public float showESFrameTime;

    CreditPart currentCredit;

    private int inputDelay = 10;
    private int delayCounter = 0;

    void Start()
    {
        currentCredit = CreditPart.Millenway;
    }

	void Update()
	{
        if (delayCounter > 0) delayCounter--;

        UpdateCreditFlow();

        if (EInput.GetButtons("Cancel", "Submit") && delayCounter == 0) {
            delayCounter = inputDelay;
            SkipCredit();
        }
	}

    void UpdateCreditFlow()
    {
        switch (currentCredit) {

            case CreditPart.Millenway:
                if (credFrame501.GetCurrentAnimatorStateInfo(0).normalizedTime >= showESFrameTime)
                {
                    credFrameES.gameObject.SetActive(true);
                    currentCredit = CreditPart.EclipseStudios;
                }
                break;


            case CreditPart.EclipseStudios:
                if (credFrameES.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f)
                    UnityEngine.SceneManagement.SceneManager.LoadScene("StartScene");
                break;
        }
    }

    void SkipCredit()
    {
        if (currentCredit == CreditPart.Millenway)
        {
            if (credFrame501.GetCurrentAnimatorStateInfo(0).normalizedTime < showESFrameTime)
            {
                string stateName = "501CreditsFade";
                credFrame501.Play(stateName, -1, showESFrameTime);
            }
        }
        else if (currentCredit == CreditPart.EclipseStudios) {
            UnityEngine.SceneManagement.SceneManager.LoadScene("StartScene");
        }
    }
}
